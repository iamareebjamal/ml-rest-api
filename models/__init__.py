from peewee import *
import os
from utils import get_root_dir

_db = None


def get_db():
    global _db
    if _db is None:
        _db = SqliteDatabase(os.path.join(get_root_dir(), 'app.db'))
    return _db


class Task(Model):
    id = UUIDField(primary_key=True)
    data = CharField(max_length=500)
    task_id = CharField()
    stage = CharField()
    status = CharField()
    progress = CharField(null=True)

    def __str__(self) -> str:
        return '<Task id={} data={} task_id={} stage={} status={} progress={}>'.format(
            self.id, self.data, self.task_id, self.stage, self.status, self.progress)

    class Meta:
        database = get_db()


if _db is None:
    get_db().connect()
    get_db().create_tables([Task])
