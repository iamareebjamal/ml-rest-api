from flask import Flask, request

from api.label import label
from api.status import get_status

import config

app = Flask(__name__)

app.config.update(**config.configuration)

app.config['MAX_CONTENT_LENGTH'] = 1000000

celery = config.celery.make_celery(app)


@app.route('/train', methods=['POST'])
def create_train():
    import tasks
    return tasks.create_training_task(request.data)


@app.route('/<task_id>/train', methods=['GET', 'POST'])
def train_task(task_id):
    import tasks
    return tasks.start_training(task_id)


@app.route('/<task_id>/label', methods=['POST'])
def label_image_request(task_id):
    return label(task_id, request)


@app.route('/<task_id>/status', methods=['GET'])
def task_status(task_id):
    return get_status(task_id)


if __name__ == "__main__":
    app.run()
