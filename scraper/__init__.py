import json
import os
import shutil
from hashlib import sha224
from threading import Thread
from utils import get_img_store_dir

import requests
from Naked.toolshed.shell import muterun_js


class Scraper:
    def __init__(self, basedir=None):
        self.js_script = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'scraper.js')
        self.urls = list()
        if basedir is None:
            self.basedir = get_img_store_dir()
        else:
            self.basedir = basedir

    def scrap(self, keyword, num):
        self._fetch_urls(keyword, num)
        self._download_all(keyword)

    def scrap_parallel(self, keyword, num):
        self._fetch_urls(keyword, num)
        self._parallel_run(self._download, keyword)

    def scrap_parallel_batch(self, keyword, num, batch_size):
        self._fetch_urls(keyword, num)
        self._batch_process_downloads(keyword, batch_size)

    def _fetch_urls(self, keyword, num):
        _command_line_agrs = '\'{} "keyword" : "{}", "num": {} {}\''.format('{', keyword, num, '}')
        _response = muterun_js(self.js_script, arguments=_command_line_agrs)
        _json_response = json.loads(_response.stdout.decode())
        self.urls = list(map(lambda value: value['url'], _json_response))
        dirname = keyword if self.basedir is None else os.path.join(self.basedir, keyword)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

    def _download(self, keyword, url):
        # generate name using uuid4
        _name = sha224(url.encode('utf-8')).hexdigest()[:10] + '.jpg'
        print('Downloading url: {}'.format(url))
        file_name = os.path.join(self.basedir, keyword, _name) \
            if self.basedir is not None \
            else os.path.join(keyword, _name)
        r = requests.head(url)
        downloaded = False
        content_length = r.headers.get('Content-Length')
        if r.status_code == 200 and content_length is not None:
            if os.path.isfile(file_name):
                print('File already exists. Checking length...')
                downloaded = int(content_length) == os.stat(file_name).st_size
        if downloaded:
            print('File is completely downloaded. Skipping...')
            return
        print('File not found or incomplete. Downloading...')
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            with open(file_name, 'wb') as f:  # Make it resumable by checking it first
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)

    def _download_all(self, keyword):
        for url in self.urls:
            self._download(keyword, url)

    def _parallel_run(self, fn, key):
        for i in self.urls:
            Thread(target=fn, args=(key, i)).start()

    def _get_next_batch(self, size):
        for i in range(0, len(self.urls), size):
            yield self.urls[i: i + size]

    def _batch_process_downloads(self, keyword, size):
        for url in self._get_next_batch(size):
            self._parallel_run_batch(self._download, keyword, url)

    @staticmethod
    def _parallel_run_batch(fn, key, urls):
        threads = []
        for url in urls:
            threads.append(Thread(target=fn, args=(key, url)))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()
