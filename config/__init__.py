from config import celery

configuration = dict(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379',
    CELERY_IMPORTS="tasks",
)
