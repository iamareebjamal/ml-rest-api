import os

from werkzeug.utils import secure_filename
from pathlib import Path

_root_dir = None


def get_root_dir():
    global _root_dir
    if _root_dir is None:
        _root_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent
    return _root_dir


def get_store_dir():
    return os.path.join(get_root_dir(), 'store')


def get_img_store_dir():
    return os.path.join(get_store_dir(), 'images')


def get_workspace_dir():
    return os.path.join(get_store_dir(), 'workspace')


def get_task_dir(task_id):
    return os.path.join(get_workspace_dir(), str(task_id))


def get_task_training_dir(task_id):
    return os.path.join(get_task_dir(task_id), 'training')


def get_task_graph(task_id):
    return os.path.join(get_task_training_dir(task_id), 'output_graph.pb')


def get_task_labels(task_id):
    return os.path.join(get_task_training_dir(task_id), 'labels.txt')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in {'jpg', '.jpeg'}


def store_image(task_id, image):
    if not allowed_file(image.filename):
        return {'error': 'Only JPEG files are supported'}
    filename = secure_filename(image.filename)
    upload_dir = os.path.join(get_task_dir(task_id), 'uploaded')
    if not os.path.exists(upload_dir):
        os.makedirs(upload_dir)
    full_path = os.path.join(upload_dir, filename)
    image.save(full_path)
    return full_path
