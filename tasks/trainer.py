import os

from train import retrainer
from tasks.task_logger import TaskLogger
from utils import get_store_dir, get_workspace_dir


class Trainer:

    def __init__(self, task_id):
        self.task_id = task_id
        self.task_logger = TaskLogger(task_id)
        self.work_dir = get_workspace_dir()

    def train(self, data=None):
        self.task_logger.save_task(stage='training', status='active')
        workspace = os.path.join(self.work_dir, self.task_id)
        training_dir = os.path.join(workspace, 'training')
        image_dir = os.path.join(workspace, 'data')
        bottleneck_dir = os.path.join(get_store_dir(), 'bottlenecks')
        intermediate_output_graph_dir = os.path.join(training_dir, 'intermediate_graph')
        output_graph = os.path.join(training_dir, 'output_graph.pb')
        output_labels = os.path.join(training_dir, 'labels.txt')
        summaries_dir = os.path.join(training_dir, 'retrain_logs')

        options = locals().copy()
        options.pop('data')
        options.pop('self')
        if data is None:
            data = {}
        retrainer.start_training({**options, **data})

        self.task_logger.save_task(stage='training', status='complete')
