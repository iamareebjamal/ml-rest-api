from models import Task

default = 0


class TaskLogger:

    def __init__(self, task_id):
        self.task_id = task_id
        self.task = Task.get(Task.id == self.task_id)

    def save_task(self, stage=None, status=None, progress=default):
        if stage is not None:
            self.task.stage = stage
        if status is not None:
            self.task.status = status
        if progress != default:
            self.task.progress = progress

        self.task.save()
