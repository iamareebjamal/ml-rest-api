import os

from random import shuffle
from shutil import copy2

from tasks.task_logger import TaskLogger
from utils import get_img_store_dir, get_workspace_dir


class Scaffolder:

    def __init__(self, task_id):
        self.task_id = task_id
        self.task_logger = TaskLogger(task_id)
        self.img_dir = get_img_store_dir()
        self.work_dir = get_workspace_dir()
        self.completed = 0

    def scaffold(self, num_images, labels):
        self.task_logger.save_task('scaffolding', 'active')

        # Make the directory
        workspace_dir = os.path.join(self.work_dir, self.task_id)
        if not os.path.isdir(workspace_dir):
            os.makedirs(workspace_dir)

        self.task_logger.save_task(progress='{}/{}'.format(self.completed, len(labels)))

        for label in labels:
            label_dir = os.path.join(workspace_dir, 'data', label)
            if not os.path.isdir(label_dir):
                os.makedirs(label_dir)

            img_label_dir = os.path.join(self.img_dir, label)

            # Copying random num_images to label dir
            all_files = [os.path.join(img_label_dir, x) for x in os.listdir(img_label_dir)]
            all_files = [x for x in all_files if os.path.isfile(x)]
            shuffle(all_files)

            num_files = all_files[:num_images]

            print('Copying {} in {}'.format(num_files, label_dir))

            for file in num_files:
                copy2(file, label_dir)

            self.completed += 1
            self.task_logger.save_task(progress='{}/{}'.format(self.completed, len(labels)))

        self.task_logger.save_task(status='complete', progress=None)
