from scraper import Scraper
from tasks.task_logger import TaskLogger


class ScraperTask:

    def __init__(self, task_id):
        self.task_logger = TaskLogger(task_id)
        self.scraper = Scraper()
        self.completed = 0

    def scrape(self, num_images, labels):
        self.task_logger.save_task('scraping', 'active')

        for label in labels:
            try:
                self.scraper.scrap_parallel_batch(label, num_images, 10)
            except Exception as e:
                print('Ignoring some error {}'.format(e))
            self.completed += 1
            self.task_logger.save_task(progress='{}/{}'.format(self.completed, len(labels)))

        self.task_logger.save_task(status='complete', progress=None)
