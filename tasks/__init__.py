import uuid
import json
from api.train import prepare_training, create_training
from tasks.task_scraper import ScraperTask
from tasks.scaffolder import Scaffolder
from tasks.trainer import Trainer

from app import celery
from models import Task


@celery.task
def train_scraping(payload):
    # Extract labels from data
    data = payload['data']
    task_id = payload['task_id']
    labels = data['labels']
    num = data['num_images']

    scraper_task = ScraperTask(task_id)
    scraper_task.scrape(num, labels)

    scaffolder = Scaffolder(task_id)
    scaffolder.scaffold(num, labels)

    trainer = Trainer(task_id)
    trainer.train(data)

    task = Task.get(id=task_id)
    task.stage = 'complete'
    task.status = 'complete'

    task.save()

    return True


@celery.task
def train_only(payload):
    task_id = payload['task_id']
    data = payload['data']

    trainer = Trainer(task_id)
    trainer.train(data)

    task = Task.get(id=task_id)
    task.status = 'complete'
    task.stage = 'complete'
    task.save()

    return True


def create_training_task(data):
    data = create_training(data)

    if isinstance(data, str):
        return data

    task_id = str(uuid.uuid4())
    task = Task(id=task_id, data=json.dumps(data), stage='queued', status='pending')
    payload = {
        'data': data,
        'task_id': task_id
    }
    result = train_scraping.delay(payload)
    task.task_id = result.task_id
    task.save(force_insert=True)
    return json.dumps({
        'status': 'queued',
        'success': True,
        'task_id': task_id
    })


def start_training(task_id, data=None):
    payload = prepare_training(task_id, data)

    if isinstance(payload, dict):
        train_only.delay(payload)
        return json.dumps({
            'status': 'success'
        })

    return payload
