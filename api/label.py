import json
import os
from models import Task
from utils import get_task_graph, get_task_labels, store_image
from train import label as label_image


def get_task_model(task: Task):
    graph = get_task_graph(task.id)
    if os.path.isfile(graph):
        return graph
    return None


def label(task_id: str, request):
    if 'file' not in request.files:
        return json.dumps({
            'error': 'No image found in request'
        })
    file = request.files['file']
    if file is None or file.filename == '':
        return json.dumps({
            'error': 'No image found in request'
        })
    if task_id is None:
        return json.dumps({
            'error': 'Task ID is required'
        })

    task = Task.get_or_none(id=task_id)

    if task is None:
        return json.dumps({
            'error': 'Task not found'
        }), 404

    model = get_task_model(task)
    labels = get_task_labels(task_id)

    if model is None:
        return json.dumps({
            'error': 'Model is not trained yet',
            'status': task.status,
            'stage': task.stage,
            'progress': task.progress
        })

    stored = store_image(task_id, file)

    if isinstance(stored, dict):
        return json.dumps(stored)

    options = {
        'image': stored,
        'graph': model,
        'labels': labels
    }

    result = label_image.label(options)

    return json.dumps(result)
