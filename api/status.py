import json
import os

from models import Task
from utils import get_task_dir, get_task_graph


def get_status(task_id: str):
    if task_id is None:
        return json.dumps({
            'error': 'Task ID is required'
        })

    task = Task.get_or_none(id=task_id)

    if task is None:
        return json.dumps({
            'error': 'Task not found'
        }), 404

    status = {
        'stage': task.stage,
        'status': task.status,
        'progress': task.progress
    }

    task_dir = get_task_dir(task_id)

    data_dir = os.path.join(task_dir, 'data')
    if os.path.isdir(data_dir):
        dirs = [os.path.join(data_dir, x) for x in os.listdir(data_dir)]
        status['scraper'] = {}
        for dir in dirs:
            file_count = len(os.listdir(dir))
            status['scraper'][os.path.basename(dir)] = file_count

    if os.path.isfile(get_task_graph(task_id)):
        status['ready'] = True

    return json.dumps(status)
