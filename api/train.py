import json
from json.decoder import JSONDecodeError
from models import Task


def prepare_training(task_id: str, data=None):
    if task_id is None:
        return json.dumps({
            'error': 'Task ID is required'
        })

    task = Task.get_or_none(id=task_id)

    if task is None:
        return json.dumps({
            'error': 'Task not found'
        }), 404

    if task.stage == 'queued' or task.stage == 'scraping' or task.stage == 'scaffolding':
        return json.dumps({
            'error': 'Task not ready for training',
            'stage': task.stage,
            'status': task.status,
            'progress': task.progress
        })

    return {'task_id': task_id, 'data': data}


def create_training(data: dict):
    if data is None:
        return json.dumps({
            'error': 'Data should be provided'
        })

    if isinstance(data, str) or isinstance(data, bytes):
        try:
            data = json.loads(str(data, 'utf-8'))
        except JSONDecodeError as e:
            return json.dumps({
                'error': 'Invalid JSON'
            })

    if 'labels' not in data:
        return json.dumps({
            'error': 'Labels not found'
        })

    if not isinstance(data['labels'], list) or len(data['labels']) < 2:
        return json.dumps({
            'error': 'There should be at least 2 labels'
        })

    if 'num_images' not in data:
        data['num_images'] = 30

    if 'training_steps' not in data:
        data['how_many_training_steps'] = 1000
    elif data['training_steps'] > 10000 or data['training_steps'] < 100:
        return json.dumps({
            'error': 'Training steps should be between 100 and 10000'
        })

    return data

